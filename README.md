# CONFAB for Jira & Confluence #



### What is CONFAB for Jira & Confluence? ###

**CONFAB** (**C**ollaboration **O**n **N**etworks **F**acilitated **A**s **B**usiness) **for Jira & Confluence** 
is a collection of whiteboard templates for **Jira** and **Confluence**.